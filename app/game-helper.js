import { Platform } from 'react-native'
import rnfs from 'react-native-fs'

export default class GameHelper {
    static _instance = null
    static instance() {
        if (this._instance == null) {
            this._instance = new GameHelper()
        }
        
        return this._instance
    }

    _categories = []
    _gre = []
    _sat = []
    _ielts = []
    _toefl = []
    _toeic = []
    _all = []

    _getGre() {
      return new Promise((resolve, rejecter) => {
        if (this._gre.length > 0) {
          resolve(this._gre)
          return
        }

        if (Platform.OS == 'ios') {
          this._gre = require('./data/gre.json')
          resolve(this._gre)
        }
        else {
          rnfs.readFileAssets('gre.json').then(res => {
            this._gre = JSON.parse(res)
            resolve(this._gre)
          })
        }
      })
    }

    _getSat() {
      return new Promise((resolve, rejecter) => {
        if (this._sat.length > 0) {
          resolve(this._sat)
          return
        }

        if (Platform.OS == 'ios') {
          this._sat = require('./data/ielts.json')
          resolve(this._sat)
        }
        else {
          rnfs.readFileAssets('ielts.json').then(res => {
            this._sat = JSON.parse(res)
            resolve(this._sat)
          })
        }
      })
    }

    _getIelts() {
      return new Promise((resolve, rejecter) => {
        if (this._ielts.length > 0) {
          resolve(this._ielts)
          return
        }

        if (Platform.OS == 'ios') {
          this._ielts = require('./data/ielts.json')
          resolve(this._ielts)
        }
        else {
          rnfs.readFileAssets('ielts.json').then(res => {
            this._ielts = JSON.parse(res)
            resolve(this._ielts)
          })
        }
      })
    }

    _getToefl() {
      return new Promise((resolve, rejecter) => {
        if (this._toefl.length > 0) {
          resolve(this._toefl)
          return
        }

        if (Platform.OS == 'ios') {
          this._toefl = require('./data/toefl.json')
          resolve(this._toefl)
        }
        else {
          rnfs.readFileAssets('toefl.json').then(res => {
            this._toefl = JSON.parse(res)
            resolve(this._toefl)
          })
        }
      })
    }

    _getToeic() {
      return new Promise((resolve, rejecter) => {
        if (this._toeic.length > 0) {
          resolve(this._toeic)
          return
        }

        if (Platform.OS == 'ios') {
          this._toeic = require('./data/toeic.json')
          resolve(this._toeic)
        }
        else {
          rnfs.readFileAssets('toeic.json').then(res => {
            this._toeic = JSON.parse(res)
            resolve(this._toeic)
          })
        }
      })
    }

    _getAll() {
      return new Promise((resolve, rejecter) => {
        if (this._all.length > 0) {
          resolve(this._all)
          return
        }

        if (Platform.OS == 'ios') {
          this._all = require('./data/word.json')
          resolve(this._all)
        }
        else {
          rnfs.readFileAssets('word.json').then(res => {
            this._all = JSON.parse(res)
            resolve(this._all)
          })
        }
      })
    }

    constructor() {
        this._categories = require('./data/categories.json')
    }

    _getCategory(code) {
        var cat = null
        this._categories.forEach(c => {
            if (code.toLowerCase() == c.code.toLowerCase()) {
                cat = c
            }
        })

        return cat
    } 

    _getWords(cat, code) {
        var words = null
        this._dictionary.forEach(d => {
            if (code.toLowerCase() == d.code.toLowerCase() && (cat == d.category || cat == "Random")) {
                words = d.values
            }
        })

        return words
    }

    _getWordSet(code0) {
      return new Promise((resolve, rejecter) => {
        switch (code0.toUpperCase()) {
          case "B": {
            this._getAll().then(res => {
              resolve(res.slice())
            })
            break
          }
          case "I": {
            this._getIelts().then(res => {
              resolve(res.slice())
            })
            break
          }
          case "S": {
            this._getSat().then(res => {
              resolve(res.slice())
            })
            break
          }
          case "T": {
            this._getToefl().then(res => {
              resolve(res.slice())
            })
            break
          }
          case "C": {
            this._getToeic().then(res => {
              resolve(res.slice())
            })
            break
          }
          case "G": {
            this._getGre().then(res => {
              resolve(res.slice())
            })
            break
          }
        }
      })
    }

    _shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex
    
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex)
          currentIndex -= 1
      
          temporaryValue = array[currentIndex]
          array[currentIndex] = array[randomIndex]
          array[randomIndex] = temporaryValue
        }
      
        return array
    }

    isGameCodeValid(code) {
      return new Promise((resolve, rejecter) => {
        if (code == null) {
          resolve(false)
        }

        const code0 = code.charAt(0)
        const category = this._getCategory(code0)

        if (category == null) {
          resolve(false)
        }

        const code1 = code.substring(1, code.length)
        const code1Index = Number.parseInt(code1)

        if (Number.isNaN(code1Index)) {
          resolve(false)
        }

        this._getWordSet(code0).then((wordSets) => {
          const posibleSetsCount = wordSets.length / 40
          if (code1Index < posibleSetsCount) {
            resolve(true)
          }

          resolve(false)
        })
      })
    }

    createNewGame(category) {
      return new Promise((resolve, rejecter) => {
        const code0 = category

        this._getWordSet(code0).then((wordSets) => {
          const code1 = Math.floor(Math.random() * (wordSets.length / 40))

          resolve(code0 + code1)
        })
      })
    }

    getCategories() {
        return this._categories
    }

    getGameDataFromCode(code) {
      return new Promise((resolve, rejecter) => {
        const code0 = code.charAt(0)

        this._getWordSet(code0).then((wordSets) => {
          const code1 = code.substring(1, code.length)
          const code1Index = Number.parseInt(code1) * 40

          const n = []

          for (let index = 0; index < 40; index++) {
              n.push(wordSets[code1Index + index])
          }

          resolve(n)
        })
      })
    }

    correctGameDataFormat(n) {
        return [ 
            [n[0], n[1], n[2], n[3], n[4]],
            [n[5], n[6], n[7], n[8], n[9]],
            [n[10], n[11], n[12], n[13], n[14]],
            [n[15], n[16], n[17], n[18], n[19]],
            [n[20], n[21], n[22], n[23], n[24]],
            [n[25], n[26], n[27], n[28], n[29]],
            [n[30], n[31], n[32], n[33], n[34]],
            [n[35], n[36], n[37], n[38], n[39]]
        ]
    }
}
