import React, {Component} from 'react'
import {
  Text, 
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native'

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height
export default class End extends Component {
    static navigationOptions = { header: null }

    code = null

    constructor(props) {
        super(props)

        this.code = props.navigation.getParam("code")
        this.isPlayOnline = props.navigation.getParam("isOnline")
    }

    _joinGame = () => {
        if (this.isPlayOnline) {
            this.props.navigation.replace('HostGameOnline', { code: this.code })
        }
        else {
            this.props.navigation.replace('HostGame', { code: this.code })
        }
    }

    _renderBackground() {
        return <ImageBackground
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                height: WINDOW_HEIGHT
            }}
            resizeMode='cover'
            source={require('../res/images/app_bg.png')}
        />
    }

    _renderLogo() {
        return <Image
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH / 2,
                height: WINDOW_WIDTH / 2,
                top: 20,
                alignSelf: 'center'
            }}
            resizeMode='center'
            source={require('../res/images/logo.png')}
        />
    }

    _renderTextContent() {
        const content = <Text
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                top: WINDOW_WIDTH / 2,
                alignSelf: 'center',
                textAlign: 'center',
                fontSize: 26,
                fontWeight: 'bold',
                color: '#040491'
            }}
        >CONGRATULATION !!!</Text>
        const separator = <View style={{ 
            width: '65%', 
            height: 0.5, 
            top: WINDOW_WIDTH / 2 + 40,
            position: 'absolute',
            alignSelf: 'center',
            backgroundColor: 'white'
        }}/>
        const mainText = <Text
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                height: WINDOW_WIDTH / 1.5,
                top: WINDOW_WIDTH / 2 + 50,
                alignSelf: 'center',
                textAlign: 'center',
                fontSize: 20,
                fontWeight: 'normal',
                color: 'black'
            }}
        >{"CREATE GROUP SUCCESS\nPLEASE SHARE THIS CODE BELOW"}</Text>
        
        return [content, separator, mainText]
    }

    _renderSharingCode() {
        return <View style={{ 
            width: '50%', 
            justifyContent: 'center', 
            height: 50, 
            backgroundColor: '#FCF0D7',
            borderRadius: 4,
            alignSelf: 'center',
            position: 'absolute',
            top: WINDOW_WIDTH / 2 + 50 + 70
        }}>
            <Text style={{ 
                textAlign: 'center',
                color: 'red',
                fontWeight: 'bold',
                fontSize: 20
            }}>
                {this.code}
            </Text>
        </View>
    }

    _renderCTA() {
        const cont = <TouchableOpacity 
        style={{
            position: 'absolute',
            width: WINDOW_WIDTH / 1.5,
            height: 100,
            alignSelf: 'center',
            top: WINDOW_WIDTH / 2 + WINDOW_WIDTH / 1.5 + 30
        }}
        activeOpacity={0.7} 
        onPress={this._joinGame}>
        <View style={{ justifyContent: 'center' }}>
            <Image 
                style={{
                    width: WINDOW_WIDTH / 1.5,
                    height: 100,
                }}
                resizeMode='contain'
                source={require('../res/images/red_button.png')}
            />
            <Text style={{
                color: 'white',
                position: 'absolute',
                textAlign: 'center',
                fontSize: 18,
                fontWeight: 'bold',
                width: WINDOW_WIDTH / 1.5
            }}>JOIN GAME</Text>
        </View>
        </TouchableOpacity>

        return [cont]
    }

    _renderCopyRight() {
        return <Text style={{
            position: 'absolute',
            bottom: 16,
            width: WINDOW_WIDTH,
            textAlign: 'center',
            color: 'white',
            fontSize: 14,
            fontWeight: 'bold'
        }}>
            POWER BY TINH HOA VIET SOLUTIONS CO.,LTD
        </Text>
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this._renderBackground()}
                {this._renderLogo()}
                {this._renderTextContent()}
                {this._renderSharingCode()}
                {this._renderCTA()}
                {this._renderCopyRight()}
            </View>
        )
    }
}
