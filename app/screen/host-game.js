import React, {Component} from 'react'
import {
  Text, 
  View,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Alert,
  ScrollView,
  BackHandler,
  Image
} from 'react-native'
import GameHelper from '../game-helper'
import DialogCombine, { SHOW_WORD_DETAIL, SHOW_LOADING } from '../component/dialog-combine'
import tts from 'react-native-tts'

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height
export default class HostGame extends Component {
    static navigationOptions = { header: null }

    gameHelper = GameHelper.instance()

    state = {
        showTag: SHOW_LOADING,
        historyWords: [],
        callingWord: "---",
        currentSelectionWord: null,
        selection: [],
        gameData: []
    }

    constructor(props) {
        super(props)

        const code = props.navigation.getParam("code")
        this.callGameIndex = 0
        this.gameHelper.getGameDataFromCode(code).then(data => {
          this.callGameData = data  
          this.setState({
            gameData: this.gameHelper.correctGameDataFormat(this._shuffle(this.callGameData.slice())),
            showTag: null
          })
        })
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this._requestQuitGame()
        return true    
    }

    _shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex
    
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex)
          currentIndex -= 1
      
          temporaryValue = array[currentIndex]
          array[currentIndex] = array[randomIndex]
          array[randomIndex] = temporaryValue
        }
      
        return array
    }

    _requestQuitGame = () => {
        Alert.alert("Quit Game", "Are you sure you want to quit this game?", [
            {text: 'Yes', onPress: () => this.props.navigation.goBack()},
            {text: 'Cancel', style: 'cancel'}
        ])
    }

    _requestCallNextWord = () => {
        const word = this.callGameData[this.callGameIndex]
        tts.setDefaultLanguage("en-US")
        tts.speak(word.word)
        this.callGameIndex += 1

        const historyWords = this.state.historyWords
        historyWords.push(word)

        this.setState({
            callingWord: word,
            historyWords
        })
    }

    _onItemSelect = (word, index) => {
        const newSelection = this.state.selection
        if (newSelection.indexOf(index) > -1) {
            this._viewWordDetail(word)
            return
        }

        if (this.state.historyWords.indexOf(word) < 0) {
            Alert.alert("Wrong word", "You just selected on the word that's not match with the required")
            return
        }

        newSelection.push(index)
        this.setState({
            selection: newSelection
        }, this._checkGameOver)    
    }

    _checkGameOver = () => {
        const selection = this.state.selection
        const results = ["0", "1", "2", "3", "4"].map(v => {
            var count = 0
            selection.forEach(s => {
                count += s.startsWith(v) ? 1 : 0
            })

            return count
        })

        if (results.indexOf(5) > -1) {
            this.props.navigation.replace('End')
        }
    }

    _getWordWithIndex = (index) => {
        if (index == null) {
            return "---"
        }

        var word = "---"
        this.state.gameData.forEach((line, l_index) => {
            line.forEach((item, i_index) => {
                const itemIndex = l_index + "" + i_index
                if (itemIndex == index) {
                    word = item
                }
            })
        })

        return word
    }

    _viewWordDetail(word) {
        if (word == "---") {
            return
        }

        this.setState({
            showTag: SHOW_WORD_DETAIL,
            currentSelectionWord: word
        })
    }

    _renderGameItem = (isSelected, content, index) => {
        return <TouchableOpacity style={{
            width: WINDOW_WIDTH / 5,
            backgroundColor: isSelected ? 'blue' : 'yellow',
            justifyContent: 'center',
            margin: 0.25
        }} activeOpacity={0.7} onPress={() => this._onItemSelect(content, index)} onLongPress={() => this._viewWordDetail(content)}>
            <Text style={{ 
                fontSize: 11,
                padding: 4,
                textAlign: 'center',
                color: isSelected ? 'white' : null
            }}>{content.word}</Text>
        </TouchableOpacity>
    }

    _renderBackground() {
        return <ImageBackground
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                height: WINDOW_HEIGHT
            }}
            resizeMode='cover'
            source={require('../res/images/app_bg.png')}
        />
    }

    _renderSelectedWord() {
        const word = this.state.callingWord
        return <TouchableOpacity style={{
            marginTop: 28,
            alignSelf: 'center',
            alignContent: 'center',
        }} activeOpacity={0.7} onPress={() => this._viewWordDetail(word)}>
          <View style={{flexDirection: 'row', alignSelf: 'center',}}>
            <Text
                style={{
                  height: 44,
                  textAlign: 'center',
                  color: '#0023a0',
                  fontWeight: 'bold',
                  fontSize: 32
                }}
            >{word == "---" ? "---" : word.word}</Text>
            <Text style={{
              height: 44,
              paddingTop: 16,
              fontWeight: 'bold',
              fontSize: 12,
              marginLeft: 8
            }}>{word == "---" ? "" : "(" + word.pron + ")"}</Text>
          </View>
          <Text style={{
              fontWeight: 'bold',
              fontSize: 12,
              marginLeft: 12
            }}>{word == "---" ? "" : "(" + word.vn_mean + ")"}</Text>
        </TouchableOpacity>
    }

    _renderHistory() {
        const items = this.state.historyWords.slice().reverse().map((v, i) => {
            return <TouchableOpacity 
                style={{
                    justifyContent: 'center',
                }}
                onPress={() => this._viewWordDetail(v)} 
                activeOpacity={0.7}    >
                <Text style={{
                    borderColor: i == 0 ? '#0023a0' : 'white',
                    borderWidth: 1,
                    marginRight: 4,
                    textAlign: 'center',
                    color: i == 0 ? '#0023a0' : 'white',
                    fontSize: 14,
                    padding: 4
                }}>{v.word}</Text>
            </TouchableOpacity>
        })
        return <ScrollView showsHorizontalScrollIndicator={false} style={{
            width: WINDOW_WIDTH - 32,
            height: 40,
            marginLeft: 12,
            marginTop: 8,
            marginBottom: 8
        }} horizontal={true}>
            <View style={{ flexDirection: 'row' }}>
                {items}
            </View>
        </ScrollView>
    }

    _renderGame = () => {
        const selection = this.state.selection
        const content = this.state.gameData.map((line, l_index) => {
            const c = line.map((item, i_index) => {
                const itemIndex = l_index + "" + i_index
                const isSelected = selection.indexOf(itemIndex) > -1
                return this._renderGameItem(isSelected, item, itemIndex)
            })
            return <View style={{ 
                flexDirection: 'row', 
                width: WINDOW_WIDTH, 
                height: 50, 
                alignSelf: 'center', 
                justifyContent: 'space-between',
            }}>
                {c}
            </View>
        })

        return content
    }

    _renderGuide() {
        return <Text
            style={{
                alignSelf: 'center',
                marginTop: 16,
                fontStyle:'italic',
                color: 'white',
                fontSize: 12
            }}
        >* long press on an item to view word's detail *</Text>
    }

    _renderCopyRight() {
        return <Text style={{
            position: 'absolute',
            bottom: 16,
            width: WINDOW_WIDTH,
            textAlign: 'center',
            color: 'white',
            fontSize: 14,
            fontWeight: 'bold'
        }}>
            POWER BY TINH HOA VIET SOLUTIONS CO.,LTD
        </Text>
    }

    _renderCTA() {
        const quitGame = <TouchableOpacity 
        style={{
            position: 'absolute',
            width: WINDOW_WIDTH / 1.5,
            height: 100,
            alignSelf: 'center',
            bottom: 0
        }}
        activeOpacity={0.7} 
        onPress={this._requestCallNextWord}>
        <View style={{ justifyContent: 'center' }}>
            <Image 
                style={{
                    width: WINDOW_WIDTH / 1.5,
                    height: 100,
                }}
                resizeMode='contain'
                source={require('../res/images/red_button.png')}
            />
            <Text style={{
                color: 'white',
                position: 'absolute',
                textAlign: 'center',
                fontSize: 18,
                fontWeight: 'bold',
                width: WINDOW_WIDTH / 1.5
            }}>CALL NEXT WORD</Text>
        </View>
        </TouchableOpacity>

        return quitGame
    }

    _renderDialogCombine() {
        return <DialogCombine 
            showTag={this.state.showTag} 
            word={this.state.currentSelectionWord}
            onClose={() => { 
                this.setState({
                    showTag: null
                })
            }}
        />
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                {this._renderBackground()}
                {this._renderSelectedWord()}
                {this._renderHistory()}
                <ScrollView style={{ marginBottom: 100 }} showsVerticalScrollIndicator={false}>
                    <View>
                        {this._renderGame()}
                        {this._renderGuide()}
                    </View>
                </ScrollView>
                {this._renderCTA()}
                {this._renderDialogCombine()}
            </View>
        )
    }
}
