import React, {Component} from 'react'
import {
  Text, 
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native'

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height
export default class End extends Component {
    static navigationOptions = { header: null }

    _requestPlayAgain = () => {
        this.props.navigation.goBack()
    }

    _renderBackground() {
        return <ImageBackground
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                height: WINDOW_HEIGHT
            }}
            resizeMode='cover'
            source={require('../res/images/app_end_bg.png')}
        />
    }

    _renderLogo() {
        return <Image
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH / 2,
                height: WINDOW_WIDTH / 2,
                top: 20,
                alignSelf: 'center'
            }}
            resizeMode='center'
            source={require('../res/images/logo.png')}
        />
    }

    _renderChampion() {
        const content = <Text
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                height: WINDOW_WIDTH / 1.5,
                top: WINDOW_WIDTH / 2,
                alignSelf: 'center',
                textAlign: 'center',
                fontSize: 26,
                fontWeight: 'bold',
                color: '#040491'
            }}
        >CONGRATULATION !!!</Text>
        const image = <Image
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH / 1.5,
                height: WINDOW_WIDTH / 1.5,
                top: WINDOW_WIDTH / 2 + 40,
                alignSelf: 'center'
            }}
            source={require('../res/images/champion.png')}
        />
        const winner = <Text
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                height: WINDOW_WIDTH / 1.5,
                top: WINDOW_WIDTH / 2 + WINDOW_WIDTH / 1.5 - 10,
                alignSelf: 'center',
                textAlign: 'center',
                fontSize: 26,
                fontWeight: 'bold',
                color: 'white'
            }}
        >WINNER</Text>
        
        return [content, image, winner]
    }

    _renderCTA() {
        const playagain = <TouchableOpacity 
        style={{
            position: 'absolute',
            width: WINDOW_WIDTH / 1.5,
            height: 100,
            alignSelf: 'center',
            top: WINDOW_WIDTH / 2 + WINDOW_WIDTH / 1.5 + 30
        }}
        activeOpacity={0.7} 
        onPress={this._requestPlayAgain}>
        <View style={{ justifyContent: 'center' }}>
            <Image 
                style={{
                    width: WINDOW_WIDTH / 1.5,
                    height: 100,
                }}
                resizeMode='contain'
                source={require('../res/images/red_button.png')}
            />
            <Text style={{
                color: 'white',
                position: 'absolute',
                textAlign: 'center',
                fontSize: 18,
                fontWeight: 'bold',
                width: WINDOW_WIDTH / 1.5
            }}>PLAY AGAIN</Text>
        </View>
        </TouchableOpacity>

        return [playagain]
    }

    _renderCopyRight() {
        return <Text style={{
            position: 'absolute',
            bottom: 16,
            width: WINDOW_WIDTH,
            textAlign: 'center',
            color: 'white',
            fontSize: 14,
            fontWeight: 'bold'
        }}>
            POWER BY TINH HOA VIET SOLUTIONS CO.,LTD
        </Text>
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this._renderBackground()}
                {this._renderLogo()}
                {this._renderChampion()}
                {this._renderCTA()}
                {this._renderCopyRight()}
            </View>
        )
    }
}
