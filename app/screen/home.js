import React, {Component} from 'react'
import {
  Text, 
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Alert,
  DeviceEventEmitter,
  ImageBackground
} from 'react-native'
import DialogCombine, { SHOW_CREATE_GROUP, SHOW_JOIN_GROUP, SHOW_LOADING } from '../component/dialog-combine'
import GameHelper from '../game-helper'
import GameOnlineHelper from '../game-online-helper'

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height
export default class Home extends Component {
    static navigationOptions = { header: null }

    dialogCombine = null
    gameHelper = GameHelper.instance()
    isConnected = false

    state = {
        showTag: null
    }

    componentDidMount() {
        GameOnlineHelper.connectServer().then(_ => {
            this.isConnected = true
        })
        .catch(_ => {
            this.isConnected = false
            Alert.alert("Error", "Can't connect to server. Please check your internet connection for online features!")
        })
    }

    _onCreateGroup = () => {
        this.setState({
            showTag: SHOW_CREATE_GROUP
        })
    }

    _onJoinGroup = () => {
        this.setState({
            showTag: SHOW_JOIN_GROUP
        })
    }

    _onDialogRedButtonClick = () => {
        const oldTag = this.state.showTag
        this.setState({
            showTag: null
        }, () => {
            setTimeout(() => {
                if (oldTag == SHOW_CREATE_GROUP) {
                    this._createGameOffline()
                }
                else if (oldTag == SHOW_JOIN_GROUP) {
                    this._joinGameOffline()
                }
            }, 500)
        })
    }

    _onDialogBlueButtonClick = () => {
        const oldTag = this.state.showTag
        this.setState({
            showTag: null
        }, () => {
            setTimeout(() => {
                if (oldTag == SHOW_CREATE_GROUP) {
                    this._createGameOnline()
                }
                else if (oldTag == SHOW_JOIN_GROUP) {
                    this._joinGameOnline()
                }
            }, 500)
        })
    }

    _joinGameOffline = () => {
      this.setState({
        showTag: SHOW_LOADING
      }, () => {
        const code = this.dialogCombine.getInvitationCode()
        this.dialogCombine.resetInvitationCode()

        this.gameHelper.isGameCodeValid(code).then(res => {
          this.setState({
            showTag: null
          }, () => {
            if (res == false) {
              Alert.alert("Bingo", "Your invitation code is not valid")
            }
            else {
              this.props.navigation.navigate('ClientGame', { code })
            }
          })
        })
      })
    }

    _createGameOffline = () => {
      this.setState({
        showTag: SHOW_LOADING
      }, () => {
        const cate = this.dialogCombine.getCategory()
        
        this.gameHelper.createNewGame(cate).then(code => {
          this.setState({
            showTag: null
          }, () => {
            this.props.navigation.navigate('GroupCreated', { code })
          })
        })
      })
    }

    _joinGameOnline = () => {
      this.setState({
        showTag: SHOW_LOADING
      }, () => {
        const code = this.dialogCombine.getInvitationCode()
        this.dialogCombine.resetInvitationCode()

        this.gameHelper.isGameCodeValid(code).then(res => {
          if (res == false) {
            this.setState({
              showTag: null
            }, () => {
              Alert.alert("Bingo", "Your invitation code is not valid")  
            })
          }
          else {
            GameOnlineHelper.joinGame(code).then(_ => {
              this.setState({
                showTag: null
              }, () => {
                this.props.navigation.navigate('ClientGameOnline', { code })
              })
            })
            .catch(_ => {
              this.setState({
                showTag: null
              }, () => {
                Alert.alert("Bingo", "There is no game with code " + code + " available")    
              })
            })
          }
        })
      })
    }

    _createGameOnline = () => {
      this.setState({
        showTag: SHOW_LOADING
      }, () => {
        const cate = this.dialogCombine.getCategory()
        this.gameHelper.createNewGame(cate).then(code => {
          GameOnlineHelper.pushGameInfo(code).then(_ => {
            this.setState({
              showTag: null
            }, () => {
              this.props.navigation.navigate('GroupCreated', { code, isOnline: true })
            })
          })
          .catch(_ => {
            this.setState({
              showTag: null
            }, () => {
              Alert.alert("Error", "Can not connect to server, please try again!")    
            })
          })
        })
      })
    }

    _renderBackground() {
        return <ImageBackground
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH,
                height: WINDOW_HEIGHT
            }}
            resizeMode='cover'
            source={require('../res/images/app_bg.png')}
        />
    }

    _renderLogo() {
        return <Image
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH / 1.5,
                height: WINDOW_WIDTH / 1.5,
                top: 40,
                alignSelf: 'center'
            }}
            resizeMode='center'
            source={require('../res/images/logo.png')}
        />
    }

    _renderCTA() {
        const createGroup = <TouchableOpacity 
            style={{
                position: 'absolute',
                width: WINDOW_WIDTH / 1.5,
                height: 100,
                alignSelf: 'center',
                top: WINDOW_WIDTH / 1.5 + 60
            }}
            activeOpacity={0.7} 
            onPress={this._onCreateGroup}>
            <View style={{ justifyContent: 'center' }}>
                <Image 
                    style={{
                        width: WINDOW_WIDTH / 1.5,
                        height: 100,
                    }}
                    resizeMode='contain'
                    source={require('../res/images/blue_button.png')}
                />
                <Text style={{
                    color: 'white',
                    position: 'absolute',
                    textAlign: 'center',
                    fontSize: 18,
                    fontWeight: 'bold',
                    width: WINDOW_WIDTH / 1.5
                }}>CREATE GROUP</Text>
            </View>
        </TouchableOpacity>

        const joinGroup = <TouchableOpacity 
        style={{
            position: 'absolute',
            width: WINDOW_WIDTH / 1.5,
            height: 100,
            alignSelf: 'center',
            top: WINDOW_WIDTH / 1.5 + 40 + 100
        }}
        activeOpacity={0.7} 
        onPress={this._onJoinGroup}>
        <View style={{ justifyContent: 'center' }}>
            <Image 
                style={{
                    width: WINDOW_WIDTH / 1.5,
                    height: 100,
                }}
                resizeMode='contain'
                source={require('../res/images/red_button.png')}
            />
            <Text style={{
                color: 'white',
                position: 'absolute',
                textAlign: 'center',
                fontSize: 18,
                fontWeight: 'bold',
                width: WINDOW_WIDTH / 1.5
            }}>JOIN GROUP</Text>
        </View>
        </TouchableOpacity>

        return [createGroup, joinGroup]
    }

    _renderCopyRight() {
        return <Text style={{
            position: 'absolute',
            bottom: 16,
            width: WINDOW_WIDTH,
            textAlign: 'center',
            color: 'white',
            fontSize: 14,
            fontWeight: 'bold'
        }}>
            POWER BY TINH HOA VIET SOLUTIONS CO.,LTD
        </Text>
    }

    _renderDialog() {
        return <DialogCombine 
            ref={ref => this.dialogCombine = ref} 
            showTag={this.state.showTag} 
            onRedButtonClick={this._onDialogRedButtonClick}
            onBlueButtonClick={this._onDialogBlueButtonClick}
        />
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this._renderBackground()}
                {this._renderLogo()}
                {this._renderCTA()}
                {this._renderCopyRight()}
                {this._renderDialog()}
            </View>
        )
    }
}
