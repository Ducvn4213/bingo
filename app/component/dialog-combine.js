import React, {Component} from 'react'
import {
  View,
  Modal,
  Image,
  Dimensions,
  Text,
  Picker,
  TextInput,
  Platform,
  NativeModules,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native'
import GameHelper from '../game-helper'
import tts from 'react-native-tts'

export const SHOW_LOADING = "SHOW_LOADING"
export const SHOW_CREATE_GROUP = "SHOW_CREATE_GROUP"
export const SHOW_JOIN_GROUP = "SHOW_JOIN_GROUP"
export const SHOW_WORD_DETAIL = "SHOW_WORD_DETAIL"

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height
export default class DialogCombine extends Component {

    gameHelper = GameHelper.instance()
    countryCode = 'en'

    constructor(props) {
        super(props)
        this.state = {
            visible: props.showTag != null,
            categorySelection: this.gameHelper.getCategories()[0].code
        }

        this.countryCode = this._getLanguageCode()
    }

    _invitationCode = null

    componentWillReceiveProps(nextProps) {
        this.setState({
            visible: nextProps.showTag != null
        })
    }

    getCategory() {
        return this.state.categorySelection
    }

    getInvitationCode() {
        return this._invitationCode
    }

    resetInvitationCode() {
        this._invitationCode = null
    }

    _renderLoading() {
      return <ActivityIndicator size='large' />
    }

    _renderButton(text, color, action) {
        const imageSource = color == 'red' ? require('../res/images/red_button.png') : require('../res/images/blue_button.png')
        return <TouchableOpacity 
            style={{
                width: 120,
                height: 50,
                alignSelf: 'center'
            }}
            activeOpacity={0.7} 
            onPress={action}>
            <View style={{ justifyContent: 'center' }}>
                <Image 
                    style={{
                        width: 120,
                        height: 50
                    }}
                    resizeMode='stretch'
                    source={imageSource}
                />
                <Text style={{
                    color: 'white',
                    position: 'absolute',
                    textAlign: 'center',
                    fontSize: 12,
                    fontWeight: 'bold',
                    width: '100%'
                }}>{text}</Text>
            </View>
        </TouchableOpacity>
    }

    _renderCategoryPicker() {
        const items = this.gameHelper.getCategories().map(c => {
            return <Picker.Item label={c.name} value={c.code} />
        })
        
        return <Picker
            selectedValue={this.state.categorySelection}
            style={{ 
                position: 'absolute',
                width: '60%',
                top: Platform.OS == 'ios' ? 0 : 90,
                alignSelf: 'center',
                backgroundColor: Platform.OS == 'ios' ? null : 'white'
             }}
            onValueChange={(itemValue) => this.setState({categorySelection: itemValue})}>
            {items}
        </Picker>
    }

    _renderCreateGroup() {
        const background = <Image 
            style={{
                width: '70%',
                height: WINDOW_WIDTH * 0.7,
                alignSelf: 'center'
            }}
            source={require('../res/images/dialog_bg.png')} 
        />
        const close = <TouchableOpacity style={{
            position: 'absolute',
            top: 12,
            right: WINDOW_WIDTH * 0.15 + 12
        }} activeOpacity={0.7} onPress={this._requestClose}>
            <Image 
                style={{
                    width: 20, 
                    height: 20
                }}
                source={require('../res/images/close.png')} />
        </TouchableOpacity>
        const categoryTitle = <Text style={{
            color: 'white',
            fontSize: 16,
            fontWeight: 'bold',
            alignSelf: 'center',
            position: 'absolute',
            top: 60
        }}>SELECT CATEGORY</Text>
        const categoryInput = this._renderCategoryPicker()
        const online = this._renderButton("CREATE ONLINE", 'blue', this.props.onBlueButtonClick)
        const offline = this._renderButton("CREATE OFFLINE", 'red', this.props.onRedButtonClick)
        const cta = <View style={{
            width: 240 + 8,
            position: 'absolute',
            flexDirection: 'row',
            alignSelf: 'center',
            justifyContent: 'space-between',
            top: WINDOW_WIDTH * 0.7 - 60
        }}>
            {[online, offline]}
        </View>
        return <View>
            {[background, close, categoryTitle, categoryInput, cta]}
        </View>
    }

    _renderJoinGroup() {
        const background = <Image 
            style={{
                width: '70%',
                height: WINDOW_WIDTH * 0.7,
                alignSelf: 'center'
            }}
            source={require('../res/images/dialog_bg.png')} 
        />
        const close = <TouchableOpacity style={{
            position: 'absolute',
            top: 12,
            right: WINDOW_WIDTH * 0.15 + 12
        }} activeOpacity={0.7} onPress={this._requestClose}>
            <Image 
                style={{
                    width: 20, 
                    height: 20
                }}
                source={require('../res/images/close.png')} />
        </TouchableOpacity>
        const codeInputTitle = <Text style={{
            color: 'white',
            fontSize: 16,
            fontWeight: 'bold',
            alignSelf: 'center',
            position: 'absolute',
            top: 60
        }}>PLEASE INPUT YOUR CODE</Text>
        const codeInput = <TextInput 
            placeholder="Invitation code"
            onChangeText={text => this._invitationCode = text}
            style={{
                position: 'absolute',
                width: '60%',
                height: 50, 
                textAlign: 'center',
                fontSize: 24,
                fontWeight: 'bold',
                top: 90,
                alignSelf: 'center',
                backgroundColor: 'white'
            }}
        />
        const online = this._renderButton("JOIN ONLINE", 'blue', this.props.onBlueButtonClick)
        const offline = this._renderButton("JOIN OFFLINE", 'red', this.props.onRedButtonClick)
        const cta = <View style={{
            width: 240 + 8,
            position: 'absolute',
            flexDirection: 'row',
            alignSelf: 'center',
            justifyContent: 'space-between',
            top: WINDOW_WIDTH * 0.7 - 60
        }}>
            {[online, offline]}
        </View>
        return <View>
            {[background, close, codeInputTitle, codeInput, cta]}
        </View>
    }

    _renderWordDetail() {
        const wordObject = this.props.word
        const wordSpell = wordObject.pron
        var description = wordObject.mean

        if (this.countryCode == 'vi' && wordObject.vn_mean) {
            description = wordObject.vn_mean
        }

        const background = <Image 
            style={{
                width: '70%',
                height: WINDOW_WIDTH * 0.7,
                alignSelf: 'center'
            }}
            source={require('../res/images/dialog_bg.png')} 
        />
        const close = <TouchableOpacity style={{
            position: 'absolute',
            top: 12,
            right: WINDOW_WIDTH * 0.15 + 12
        }} activeOpacity={0.7} onPress={this._requestClose}>
            <Image 
                style={{
                    width: 20, 
                    height: 20
                }}
                source={require('../res/images/close.png')} />
        </TouchableOpacity>
        const word = <Text style={{
            color: 'white',
            fontSize: 20,
            fontWeight: 'bold',
            width: WINDOW_WIDTH * 0.7 - 32, 
            left: WINDOW_WIDTH * 0.15 + 16,
            position: 'absolute',
            top: 50
        }}>{wordObject.word}</Text>
        const speaker = <TouchableOpacity style={{
            width: 20, 
            height: 20,
            borderRadius: 2,
            justifyContent: 'center',
            backgroundColor: 'white'
        }} activeOpacity={0.7} onPress={() => {
            tts.setDefaultLanguage("en-US")
            tts.speak(wordObject.word)
        }}>
            <Image
                style={{
                    width: 16, 
                    height: 16,
                    alignSelf: 'center'
                }}
                source={require('../res/images/speaker.png')}
            />
        </TouchableOpacity>
        const spell = <Text style={{
            color: 'white',
            marginLeft: 8,
            fontSize: 14
        }}>{wordSpell}</Text>

        const wordBaseInfo = <View style={{
            position: 'absolute',
            left: WINDOW_WIDTH * 0.15 + 16,
            top: 80,
            flexDirection: 'row'
        }}>
            {[speaker, spell]}
        </View>

        const desc = <Text style={{
            position: 'absolute',
            left: WINDOW_WIDTH * 0.15 + 16,
            height: WINDOW_WIDTH * 0.7 - 110 - 16,
            top: 110,
            color: 'white',
            width: WINDOW_WIDTH * 0.7 - 32,
            fontSize: 14
        }}>{description}</Text>

        return <View>
            {[background, close, word, wordBaseInfo, desc]}
        </View>
    }

    _renderContent = () => {
        switch(this.props.showTag) {
            case SHOW_LOADING: return this._renderLoading()
            case SHOW_CREATE_GROUP: return this._renderCreateGroup()
            case SHOW_JOIN_GROUP: return this._renderJoinGroup()
            case SHOW_WORD_DETAIL: return this._renderWordDetail()
        }
    }
    
    _requestClose = () => {
        this.setState({ visible: false })
        if (this.props.onClose) {
            this.props.onClose()
        }
    }

    _getLanguageCode() {
        let systemLanguage = 'en'
        if (Platform.OS === 'android') {
          systemLanguage = NativeModules.I18nManager.localeIdentifier
        } else {
          systemLanguage = NativeModules.SettingsManager.settings.AppleLocale
        }
        const languageCode = systemLanguage.substring(0, 2)
        return languageCode
    }

    render() {
        return (
            <Modal
                animationType='fade'
                transparent={true}
                onRequestClose={() => {}}
                visible={this.state.visible}>
                <View style={{
                    flex: 1,
                    justifyContent: 'center', 
                    backgroundColor: 'rgba(0, 0, 0, 0.5)'
                }} >
                    <TouchableOpacity 
                        activeOpacity={1} 
                        style={{
                            position: 'absolute',
                            width: WINDOW_WIDTH,
                            height: WINDOW_HEIGHT
                        }} 
                        onPress={this._requestClose}/>
                    {this._renderContent()}
                </View>
            </Modal>
        )
    }
}