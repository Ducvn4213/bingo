import React from 'react'
import { 
  View,
  Easing,
  Animated,
  Platform
} from 'react-native'
import { createStackNavigator } from 'react-navigation'
import Home from './screen/home'
import End from './screen/end'
import HostGame from './screen/host-game'
import HostGameOnline from './screen/host-game-online'
import ClientGame from './screen/client-game'
import ClientGameOnline from './screen/client-game-online'
import GroupCreated from './screen/group-created'

const transitionConfig = () => {
  return {
    transitionSpec: {
        duration: 700,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing,
        useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {      
        const { position, scene } = sceneProps
        const thisSceneIndex = scene.index

        const opacity = position.interpolate({
            inputRange: [thisSceneIndex - 1, thisSceneIndex],
            outputRange: [0, 1],
        })

        return { opacity } 
    }
  }
}

const androidScreensDefination = {
  Home: { screen: Home},
  GroupCreated: { screen: GroupCreated},
  HostGame: { screen: HostGame},
  HostGameOnline: { screen: HostGameOnline },
  ClientGame: { screen: ClientGame},
  ClientGameOnline: { screen: ClientGameOnline },
  End: { screen: End}
}

const iosScreensDefination = {
  Home: { screen: Home},
  GroupCreated: { screen: GroupCreated},
  HostGame: { screen: HostGame},
  HostGameOnline: { screen: HostGameOnline },
  ClientGame: { screen: ClientGame},
  ClientGameOnline: { screen: ClientGameOnline },
  End: { screen: End}
}

const iosNavigationOptions = {
  transitionConfig, 
  navigationOptions: {
      gesturesEnabled: false
  } 
}

const androidNavigationOptions = { 
  navigationOptions: {
      gesturesEnabled: false
  } 
}

const screensDefination = Platform.OS == 'ios' ? iosScreensDefination : androidScreensDefination
const navigationOptions = Platform.OS == 'ios' ? iosNavigationOptions : androidNavigationOptions

const TheNavigator = createStackNavigator(screensDefination, navigationOptions)

export default class App extends React.Component {
  constructor(props) {
    super(props)

    console.disableYellowBox = true
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <TheNavigator />
      </View>
    )
  }
}