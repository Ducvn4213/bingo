package com.bingo.core;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class GameHelperModule extends ReactContextBaseJavaModule {

  public GameHelperModule(final ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  public String getName() {
    return "GameHelperModule";
  }

  @ReactMethod
  public void connectServer(Promise promise) {
    promise.resolve(true);
  }

  @ReactMethod
  public void pushGameInfo(String code, Promise promise) {
    FRDSupport.getInstance(getReactApplicationContext()).setNewGame(code.toUpperCase(), promise);
  }

  @ReactMethod
  public void setCallingWordForGame(String gameCode, String newWord, Promise promise) {
    FRDSupport.getInstance(getReactApplicationContext()).setCallingWord(gameCode.toUpperCase(), newWord, promise);
  }

  @ReactMethod
  public void joinGame(String gameCode, Promise promise) {
    FRDSupport.getInstance(getReactApplicationContext()).joinGame(gameCode.toUpperCase(), promise);
  }

  @ReactMethod
  public void quitGame(String gameCode) {
    FRDSupport.getInstance(getReactApplicationContext()).quitGame(gameCode.toUpperCase());
  }

  @ReactMethod
  public void gameWin(String gameCode, Promise promise) {
    FRDSupport.getInstance(getReactApplicationContext()).setGameWinner(gameCode.toUpperCase(), promise);
  }
}
