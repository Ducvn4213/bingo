package com.bingo.core;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameHelperPackage implements ReactPackage {

  @Override
  public List<NativeModule> createNativeModules(final ReactApplicationContext reactContext) {
    List<NativeModule> modules = new ArrayList<>();

    modules.add(new GameHelperModule(reactContext));

    return modules;
  }

  @Override
  public List<ViewManager> createViewManagers(final ReactApplicationContext reactContext) {
    return Collections.emptyList();
  }
}
