package com.bingo.core;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

class FRDSupport {

  private static FRDSupport instance;

  static FRDSupport getInstance(ReactContext context) {
    if (instance == null) {
      instance = new FRDSupport(context);
    }

    instance.context = context;
    return instance;
  }

  private DatabaseReference bingoDB;
  private ReactContext context;

  private FRDSupport(ReactContext context) {
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    this.bingoDB = database.getReference("game");
    this.context = context;
  }

  private ChildEventListener childEventListener = new ChildEventListener() {
    @Override
    public void onChildAdded(@NonNull final DataSnapshot dataSnapshot, @Nullable final String s) {
      String key = dataSnapshot.getKey();
      if (key == null) {
        return;
      }

      if (key.equalsIgnoreCase("calling")) {
        sendEvent("next-calling-word", (String) dataSnapshot.getValue());
        return;
      }

      if (key.equalsIgnoreCase("winner") && (boolean) dataSnapshot.getValue()) {
        sendEvent("winner", "true");
      }
    }

    @Override
    public void onChildChanged(@NonNull final DataSnapshot dataSnapshot, @Nullable final String s) {
      String key = dataSnapshot.getKey();
      if (key == null) {
        return;
      }

      if (key.equalsIgnoreCase("calling")) {
        sendEvent("next-calling-word", (String) dataSnapshot.getValue());
        return;
      }

      if (key.equalsIgnoreCase("winner") && (boolean) dataSnapshot.getValue()) {
        sendEvent("winner", "true");
      }
    }

    @Override
    public void onChildRemoved(@NonNull final DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull final DataSnapshot dataSnapshot, @Nullable final String s) {

    }

    @Override
    public void onCancelled(@NonNull final DatabaseError databaseError) {

    }
  };

  void setNewGame(String code, final Promise promise) {
    bingoDB.child(code).child("code").setValue(code, new CompletionListener() {
      @Override
      public void onComplete(
          @Nullable final DatabaseError databaseError, @NonNull final DatabaseReference databaseReference) {
        if (databaseError == null) {
          promise.resolve(null);
        } else {
          promise.reject("new-game", "can not create new game");
        }
      }
    });
  }

  void joinGame(final String code, final Promise promise) {
    bingoDB.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChild(code)) {
          bingoDB.child(code).addChildEventListener(childEventListener);
          promise.resolve(null);
        } else {
          promise.reject("join-game", "no code game available");
        }
      }

      @Override
      public void onCancelled(@NonNull final DatabaseError databaseError) {}
    });
  }

  void quitGame(String code) {
    bingoDB.child(code).removeEventListener(childEventListener);
  }

  void setCallingWord(String code, String word, final Promise promise) {
    bingoDB.child(code).child("calling").setValue(word, new CompletionListener() {
      @Override
      public void onComplete(
          @Nullable final DatabaseError databaseError, @NonNull final DatabaseReference databaseReference) {
        if (databaseError == null) {
          promise.resolve(null);
        } else {
          promise.reject("set-calling", "can not set new calling word");
        }
      }
    });
  }

  void setGameWinner(String code, final Promise promise) {
    bingoDB.child(code).child("winner").setValue(true, new CompletionListener() {
      @Override
      public void onComplete(
          @Nullable final DatabaseError databaseError, @NonNull final DatabaseReference databaseReference) {
        if (databaseError == null) {
          promise.resolve(null);
        } else {
          promise.reject("set-winner", "can not set winner");
        }
      }
    });
  }

  private void sendEvent(String key, String nextCalling) {
    WritableMap params = Arguments.createMap();
    params.putString("word", nextCalling);

    this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
        .emit(key, params);
  }
}
