
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
@import Firebase;

@interface GameHelperModule : RCTEventEmitter<RCTBridgeModule>

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property FIRDatabaseHandle handler;
@property Boolean isListening;

@end
