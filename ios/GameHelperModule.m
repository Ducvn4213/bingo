//
//  GameHelperModule.m
//  Bingo
//
//  Created by LAP00709 on 2/1/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "GameHelperModule.h"

@implementation GameHelperModule

- (instancetype)init
{
  self = [super init];
  if (self) {
    _ref = [[FIRDatabase database] reference];
    _isListening = false;
  }
  return self;
}

RCT_EXPORT_MODULE();

- (NSArray<NSString *> *)supportedEvents
{
  return @[@"next-calling-word", @"winner"];
}

RCT_EXPORT_METHOD(connectServer:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  resolve(@"ok");
}

RCT_EXPORT_METHOD(pushGameInfo:(NSString*)code resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  [[[_ref child:@"game"] child:code] setValue:@{@"code": code} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
    if (error) {
      reject(@"new-game", @"can not create new game", error);
    }
    else {
      resolve(@"good");
    }
  }];
}

RCT_EXPORT_METHOD(setCallingWordForGame:(NSString*)code newWord:(NSString*)newWord resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  [[[_ref child:@"game"] child:code] updateChildValues:@{@"calling": newWord} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
    if (error) {
      reject(@"set-calling", @"can not set new calling word", error);
    }
    else {
      resolve(@"good");
    }
  }];
}

- (void) sendEventString:(NSString*)name value:(NSString*)value {
  [self sendEventWithName:name body:value];
}

- (void) sendEventNumber:(NSString*)name value:(NSNumber*)value {
  [self sendEventWithName:name body:value];
}

RCT_EXPORT_METHOD(joinGame:(NSString*)code resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  _handler = [[[_ref child:@"game"] child:code] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
    NSString *callingWord = snapshot.value[@"calling"];
    [self sendEventString:@"next-calling-word" value:callingWord];
    
    if ([snapshot.value[@"winner"] isEqual: @true]) {
      [self sendEventNumber:@"winner" value:@true];
    }
    if (self->_isListening == false) {
      self->_isListening = true;
      resolve(@"good");
    }
  } withCancelBlock:^(NSError * _Nonnull error) {
    if (self->_isListening == false) {
      self->_isListening = true;
      reject(@"join-game", @"no code game available", error);
    }
  }];
}

RCT_EXPORT_METHOD(quitGame:(NSString*)code)
{
  [_ref removeObserverWithHandle:_handler];
  _isListening = false;
}

RCT_EXPORT_METHOD(gameWin:(NSString*)code resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
  [[[_ref child:@"game"] child:code] updateChildValues:@{@"winner": @true} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
    if (error) {
      reject(@"set-winner", @"can not set winner", error);
    }
    else {
      resolve(@"good");
    }
  }];
}

@end
